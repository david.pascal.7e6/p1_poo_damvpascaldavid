﻿using System;

namespace DavidPascal7e5M03UF4
{
    public class Program
    {

    }
    class FiguraGeometrica
    {
        public int Codi { get; set; }
        public string Nom { get; set; }
        public ConsoleColor Color { get; set; }
    }
    class Rectangle
    {
        public double Base { get; set; }
        public double Altura { get; set; }
    }
    class Triangle
    {
        public double Base { get; set; }
        public double Altura { get; set; }
    }
    class Cercle
    {
        public double Radi { get; set; }
    }
}
